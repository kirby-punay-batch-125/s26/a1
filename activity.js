db.course_bookings.insertMany([
	{"courseID": "C001", "studentId": "S004", "isCompleted": true},
	{"courseID": "C002", "studentId": "S001", "isCompleted": false},
	{"courseID": "C001", "studentId": "S003", "isCompleted": true},
	{"courseID": "C003", "studentId": "S002", "isCompleted": false},
	{"courseID": "C001", "studentId": "S002", "isCompleted": true},
	{"courseID": "C004", "studentId": "S003", "isCompleted": false},
	{"courseID": "C002", "studentId": "S004", "isCompleted": true},
	{"courseID": "C003", "studentId": "S007", "isCompleted": false},
	{"courseID": "C001", "studentId": "S005", "isCompleted": true},
	{"courseID": "C004", "studentId": "S008", "isCompleted": false},
	{"courseID": "C001", "studentId": "S013", "isCompleted": true},
]);


db.course_bookings.aggregate(
			[
				{$match: {"isCompleted": true}},
				{$group: {_id: null, total: {$sum: 1}}}
			]
		);

db.course_bookings.aggregate(
			[
				{$match: {"isCompleted": false}},
				{$project: {"courseID": 0}}
			]
		);

db.course_bookings.aggregate(
			[
				{$sort: {"courseID": -1, "studentId": 1}},			
			]
		);




